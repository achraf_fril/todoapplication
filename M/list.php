<style>
    .time{
        display:flex;
        align-items:center;
        opacity: 0.5;
    }
</style>
<?php if(isset($required)){?>
<?php $table = lister_table()?>
<ul class="list-group">
<?php  foreach ($table as $row):?>
    <?php 
        if($row["done"]== 0){
            $bg = "list-group-item-warning";
        }
        else{
            $bg = "list-group-item-success"
        ;}
        ?>
    <li class="list-group-item d-flex justify-content-between align-items-center <?= $bg ?>">
        <div><?= $row["title"]?></div>
        <div class="btn-group" role="group">
            <div class="time">
            <?= $row["created_at"]?>
            </div>
            <?php 
            $done = $row["done"];
            $word = ($done==0) ? "<i class='bi bi-check'></i> done": "<i class='bi bi-arrow-counterclockwise'></i> undo";
            ?>
            <form action="operation.php" method="POST">
                <input type="hidden" name="tache_id" value="<?= $row["id"]?>">
                <button class="btn btn" type="submit" name="action" value="toggle" ><?= $word?></button>
            </form>
            <form action="operation.php" method="POST">
                <input type="hidden" name="tache_id" value="<?= $row["id"]?>">
                <button class="btn btn-danger" type="submit" name="action" value="delete"><i class="bi bi-trash3-fill"></i></butoon>
            </form>
        </div>
        </li>
<?php endforeach;
?>
</table>
<?php }else{
    header("location:deniedpage.php");
}?>


