<?php 
function toggle_done($tache_id){
    global $conn;
        $sql = "UPDATE todo SET done = 1- done WHERE id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $tache_id);
        $stmt->execute();
        return $stmt;
}

function new_task($title){
    global $conn;
    $sql = "INSERT INTO todo (title) values (:title)";
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(":title",$title);
    if($stmt->execute()){
        return true;
    }}

function delete($tache_id){
        global $conn;
        $sql = "DELETE from todo WHERE id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":id",$tache_id);
        if($stmt->execute()){
            return true;
}}

function lister_table(){
    global $conn;
    $sql = "SELECT * FROM todo ORDER BY created_at DESC";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchall(PDO::FETCH_ASSOC);
    return $result;
}
?>
